package app.com.example.anddroid.kalkulator;

//класс собирающий число с плавающей точкой

import java.text.DecimalFormat;

public class NumBuilder {
    private StringBuilder numInString = new StringBuilder("0");
    private int maxLength = 5;
    private boolean isNegative = false;//использую эту переменную вместо добавления "-" в начало numInString,
                                        //чтобы избавиться от нескольких ифов внутри метода append
    private boolean isInteger = true;//целая ли часть числа сейчас обрабатывается


    public void append(char digit){
        if (isInteger){
            if(numInString.length() == 1 && numInString.charAt(0) == '0' ) {//чтобы не было кучи бесполезных нулей
                numInString.setCharAt(0, digit);
            }else {
                if (numInString.length() < maxLength)
                    numInString.append(digit);
            }
        }else{
            if(numInString.length() < maxLength){
                numInString.append(digit);
            }
        }
    }

    public void changeSign(){//меняем знак
        isNegative = !isNegative;
    }

    public void addDot(){
        if(isInteger && numInString.length() < maxLength - 1){
            numInString.append('.');
            isInteger = false;
        }
    }

    public void deleteLast(){
        if (numInString.length() > 1)
            numInString.deleteCharAt(numInString.length() - 1);
        else{
            if (numInString.length() == 1) numInString.setCharAt(0, '0');
        }
    }

    public boolean isEmpty(){
        return numInString.length() == 0;
    }

    public double getValue(){
            double value = Double.parseDouble(numInString.toString());
            return !isNegative ? value : value * (-1);
    }

    @Override
    public String toString() {
        if (isNegative)
            return '-' + numInString.toString();
        else
            return numInString.toString();
    }

    public static String format(Double value){
        DecimalFormat myFormatter = new DecimalFormat("#######.######");
        return myFormatter.format(value);
    }
}
