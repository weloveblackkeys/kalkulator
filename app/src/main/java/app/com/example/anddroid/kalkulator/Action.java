package app.com.example.anddroid.kalkulator;

/*переисление, которое храниит все виды действий, которые могут быть выполнены*/
public enum Action {

    plus, minus, multiply, divide, ravno;


    @Override
    public String toString() {
        switch (this.ordinal()){
            case 0:
                return " + ";
            case 1:
                return " - ";
            case 2:
                return " * ";
            case 3:
                return " / ";
            case 4:
                return " = ";
            default: return null;
        }
    }

}
