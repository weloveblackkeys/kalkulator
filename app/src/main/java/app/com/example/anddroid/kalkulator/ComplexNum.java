package app.com.example.anddroid.kalkulator;


public class ComplexNum{

    private double rePart;
    private double imPart;

    public ComplexNum(double rePart, double imPart){
        this.rePart = rePart;
        this.imPart = imPart;
    }

    public ComplexNum(){
        this.rePart = 0;
        this.imPart = 0;
    }

    public double getRePart() {
        return rePart;
    }

    public void setRePart(double rePart) {
        this.rePart = rePart;
    }

    public double getImPart() {
        return imPart;
    }

    public void setImPart(double imPart) {
        this.imPart = imPart;
    }

    @Override
    public String toString(){
        if (imPart >= 0)
            return '(' + NumBuilder.format(rePart) + '+' + NumBuilder.format(imPart) + 'i' + ')';
        else
            return '(' + NumBuilder.format(rePart) + NumBuilder.format(imPart) + 'i' + ')';
    }

    public ComplexNum plus(ComplexNum secondNum){
        return new ComplexNum(this.getRePart() + secondNum.getRePart(), this.getImPart() + secondNum.getImPart());
    }

    public ComplexNum minus(ComplexNum secondNum){
        return new ComplexNum(this.getRePart() - secondNum.getRePart(), this.getImPart() - secondNum.getImPart());
    }

    public ComplexNum multiply(double secondNum){
        return new ComplexNum(this.getRePart() * secondNum, this.getImPart() * secondNum);
    }

    public ComplexNum multiply(ComplexNum secondNum){
        double resRePart = this.getRePart() * secondNum.getRePart() - this.getImPart() * this.getImPart();
        double resImPart = this.getRePart() * secondNum.getImPart() + this.getImPart() * secondNum.getRePart();
        return new ComplexNum(resRePart, resImPart);
    }

    public ComplexNum divide(double secondNum){
        return new ComplexNum(this.getRePart() / secondNum, this.getImPart() / secondNum);
    }

    public ComplexNum divide(ComplexNum secondNum){
        return this.multiply(new ComplexNum(secondNum.getRePart(), secondNum.getImPart() * (-1)))
                .divide(secondNum.getRePart() * secondNum.getRePart() +
                        secondNum.getImPart() * secondNum.getImPart());
    }

    public ComplexNum doIt(Action action, ComplexNum secondNum){
        switch (action){
            case plus:
                return plus(secondNum);
            case minus:
                return minus(secondNum);
            case multiply:
                return multiply(secondNum);
            case divide:
                return divide(secondNum);
            case ravno:
                return this;
            default:
                return null;
        }
    }

}
