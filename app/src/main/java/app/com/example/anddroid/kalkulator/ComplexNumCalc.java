package app.com.example.anddroid.kalkulator;

import java.util.LinkedList;
import java.util.List;
/*класс калькулятор, хранит переменные и дествия которые должны быть с ними выполнены*/

public class ComplexNumCalc {

    private List<ComplexNumAndAction> nums = new LinkedList<ComplexNumAndAction>();

    public void add(ComplexNumAndAction newNum){
        nums.add(newNum);
    }

    @Override
    public String toString() {
        StringBuilder res = new StringBuilder();
        for (ComplexNumAndAction num : nums) {
            res.append(num.toString());
        }
        return res.toString();
    }

    public ComplexNumAndAction calculate(){
        if (nums.size() == 0) return null;
        if (nums.size() == 1) return nums.get(0);
        int i = 0;
        while(i < nums.size() - 2) {//сначала считаем делние и умножение
            if(nums.get(i).action == Action.multiply || nums.get(i).action == Action.divide){
                nums.set(i, nums.get(i).doAction(nums.get(i + 1)));
                nums.remove(i + 1);
            }
            else ++i;
        }
        if (nums.size() == 1) return nums.get(0);
        ComplexNumAndAction res = nums.get(0);
        i = 1;
        while (i < nums.size() || nums.get(i - 1).getAction() != Action.ravno) {//подсчет заканивается
            res = res.doAction(nums.get(i));                                    //концом списка
            ++i;                                                                //либо действием равно
        }
        return res;
    }

    public void clear(){
        nums.clear();
    }

}
