package app.com.example.anddroid.kalkulator;

/*класс действия над комплексным числом, хранит число и действие, которое необходимо над ним выполнить
* Правильнее было бы не наследовать класс ComplexNum, а объявить переменную этого класса в данном классе,
* но все итак хорошо работает, так что не вижу смысла все переписывать*/

public class ComplexNumAndAction extends ComplexNum {

    Action action;

    public ComplexNumAndAction(double rePart, double imPart, Action action) {
        super(rePart, imPart);
        this.action = action;
    }

    public ComplexNumAndAction(ComplexNum num, Action action){
        super(num.getRePart(), num.getImPart());
        this.action = action;
    }

    public ComplexNumAndAction(){
        super();
    }

    public void setAction(Action action){
        this.action = action;
    }

    public Action getAction() {
        return action;
    }

    public ComplexNum doAction(ComplexNum secondNum){
        return super.doIt(action, secondNum);
    }

    public ComplexNumAndAction doAction(ComplexNumAndAction secondNum){
        ComplexNum res = super.doIt(action, secondNum);
        return new ComplexNumAndAction(res, secondNum.getAction());
    }

    @Override
    public String toString() {
        return super.toString() + action.toString();
    }

    public String numToString(){
        return  super.toString();
    }
}

