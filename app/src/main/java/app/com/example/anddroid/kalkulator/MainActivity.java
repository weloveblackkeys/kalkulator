package app.com.example.anddroid.kalkulator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView mainTextView;
    private TextView secondTextView;
    private TextView reImTextView;//отображается какя часть исла сейчас вводится

    private ComplexNumAndAction complexNum = new ComplexNumAndAction();

    private ComplexNumCalc calc = new ComplexNumCalc();
    private NumBuilder num = new NumBuilder();
    private boolean isIm = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainTextView = (TextView) findViewById(R.id.textViewMain);
        secondTextView = (TextView) findViewById(R.id.textViewSecond);
        reImTextView = (TextView) findViewById(R.id.textViewReIm);
    }

    public void clickOnNum(View view){
        num.append(((Button) view).getText().charAt(0));
        display();
    }

    public void clickOnDot(View view){
        num.addDot();
        display();
    }

    public void clickOnIm(View view){
        if (!isIm) {
            complexNum.setRePart(num.getValue());
            num = new NumBuilder();
            reImTextView.setText("Im");
            isIm = true;
        }
    }


    public void clickOnAction(View view){
        Action action = Action.valueOf(view.getContentDescription().toString());
        if (isIm) {
            complexNum.setImPart(num.getValue());
        } else {
            complexNum.setImPart(0);
            complexNum.setRePart(num.getValue());
        }

        complexNum.setAction(action);
        calc.add(complexNum);
        /*complexNum = new ComplexNumAndAction();
        num = new NumBuilder();
        isIm = false;*/

        if (action == Action.ravno){
            secondTextView.setText(calc.toString());
            mainTextView.setText( calc.calculate().numToString());
            calc.clear();
        }else {
            mainTextView.setText(calc.toString());;
        }
        clearVariables();
        reImTextView.setText("Re");
    }

    public void display(){
        if (!isIm){
            mainTextView.setText(calc.toString() + '(' + num.toString() + '+' + "0i)");
            reImTextView.setText("Re");
        }else {
            String str =  num.toString();
            if (str.charAt(0) != '-') str = '+' + num.toString();
            mainTextView.setText(calc.toString() + '(' + NumBuilder.format(complexNum.getRePart()) + str + "i)");
        }
    }

    public void clickOnC(View view){
        calc.clear();
        clearVariables();
        secondTextView.setText("");
        mainTextView.setText("");
        reImTextView.setText("");
    }


    public void clickOnDel(View view){
        num.deleteLast();
        display();
    }

    public void clickOnPlusMinus(View view){
        num.changeSign();
        display();
    }

    private void clearVariables(){
        isIm = false;
        num = new NumBuilder();
        complexNum = new ComplexNumAndAction();
    }

}
